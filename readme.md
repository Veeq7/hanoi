## Tower of Hanoi
Implementation of hanoi towers in go lang.

## Compile
Use `go build` to compile the project

## Usage
- Default run<br>`hanoi` 
- Help menu<br>`hanoi -h`
- Customized parameters<br>`hanoi -t 5 -l 64`