package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func askInt(text string) int {
	fmt.Print(text)
	reader := bufio.NewReader(os.Stdin)
	str, err := reader.ReadString('\n')
	if err != nil {
		log.Fatal(err.Error())
	}
	i, err := strconv.ParseInt(strings.Trim(str, "\r\n"), 10, 0)
	if err != nil {
		return -1
	}
	return int(i)
}

func cls() {
	c := exec.Command("cmd", "/c", "cls")
	c.Stdout = os.Stdout
	c.Run()
}

func printTowers(hanoi towers) {
	for n, tower := range hanoi {
		print("Tower ", n, ": ")
		for _, layer := range tower {
			print(layer, " ")
		}
		println()
	}
}

type towers [][]int

var towerCount int
var layerCount int

func init() {
	flag.IntVar(&towerCount, "t", 3, "Specifies amount of towers")
	flag.IntVar(&layerCount, "l", 8, "Specifies amount of layers")
	flag.Parse()

	if towerCount < 1 {
		log.Fatalln("Invalid tower count")
	}
	if layerCount < 1 {
		log.Fatalln("Invalid layer count")
	}
}

func main() {
	hanoi := towers{}
	for i := 0; i < towerCount; i++ {
		hanoi = append(hanoi, []int{})
	}
	for i := layerCount; i > 0; i-- {
		hanoi[0] = append(hanoi[0], i)
	}
	cls()
	fmt.Println()
	for {
		win, str := func() (bool, string) {
			printTowers(hanoi)

			src := askInt("Source Tower: ")
			if src < 0 || src >= towerCount || len(hanoi[src]) <= 0 {
				return false, "Invalid source!"
			}

			tgt := askInt("Target Turret: ")
			if src < 0 || src >= towerCount {
				return false, "Invalid target!"
			}

			{
				srcTower := hanoi[src]
				tgtTower := hanoi[tgt]
				srcValue := srcTower[len(srcTower)-1]
				tgtValue := layerCount + 1 // empty tower always allows moves
				if len(tgtTower) > 0 {
					tgtValue = tgtTower[len(tgtTower)-1]
				}

				if srcValue < tgtValue {
					hanoi[src] = srcTower[:len(srcTower)-1]
					hanoi[tgt] = append(tgtTower, srcValue)
				} else {
					return false, "Can't move there!"
				}
			}

			for i := 1; i < towerCount; i++ {
				if len(hanoi[i]) == layerCount {
					return true, "You are victorious"
				}
			}

			return false, ""
		}()

		cls()
		fmt.Println(str)

		if win {
			break
		}
	}
}
